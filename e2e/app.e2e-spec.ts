import { MakeyourselfPage } from './app.po';

describe('makeyourself App', function() {
  let page: MakeyourselfPage;

  beforeEach(() => {
    page = new MakeyourselfPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
